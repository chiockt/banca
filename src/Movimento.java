import java.time.LocalDateTime;

public class Movimento {
    private LocalDateTime dataRichiesta;
    private LocalDateTime dataValuta;
    private String causale;
    private String tipologia;
    private double importo;

    public Movimento(LocalDateTime dataRichiesta, LocalDateTime dataValuta, String causale, String tipologia, double importo) {
        this.dataRichiesta = dataRichiesta;
        this.dataValuta = dataValuta;
        this.causale = causale;
        this.tipologia = tipologia;
        this.importo = importo;
    }

    public LocalDateTime getDataRichiesta() {
        return dataRichiesta;
    }

    public LocalDateTime getDataValuta() {
        return dataValuta;
    }

    public String getCausale() {
        return causale;
    }

    public String getTipologia() {
        return tipologia;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }

    public String toString() {
        String dataRichiestaStr = dataRichiesta.toString();
        String dataValutaStr = dataValuta.toString();

        return "Data Richiesta: " + dataRichiestaStr +
               "\nData Valuta: " + dataValutaStr +
               "\nCausale: " + causale +
               "\nTipologia: " + tipologia +
               "\nImporto: " + importo;
    }
}
