import java.util.Scanner;

public class TerminaleBanca {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Persona intestatario = new Persona("Thomas", "Chiocchetti", "CHCTMS..."); 
        ContoCorrente cSiric = new ContoCorrente(2500, intestatario);

        System.out.println("Saldo: " + cSiric.getSaldo());

        System.out.println("Inserisci \n1.Per prelevare\n2. Per depositare");
        int scelta = scanner.nextInt();

        scanner.nextLine(); 

        System.out.println("Inserisci la causale dell'operazione:");
        String causale = scanner.nextLine();

        switch (scelta) {
            case 1:
                System.out.println("Inserisci l'importo da prelevare: ");
                double importoPrelievo = scanner.nextDouble();
                cSiric.prelievo(importoPrelievo, causale);
                break;

            case 2:
                System.out.println("Inserisci l'importo da depositare: ");
                double importoDeposito = scanner.nextDouble();
                cSiric.deposito(importoDeposito, causale);
                break;

            default:
                System.out.println("Scelta non valida.");
        }

        cSiric.stampaMovimenti();
    }
}